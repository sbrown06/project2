from matplotlib import pyplot as plt
from matplotlib import animation as a
import numpy as np
class Car():
	
	def __init__(self, x=0, dx=1, bumper = None):
		self.x = x
		self.bumper = bumper
		self.dx = dx
	def __repr__(self, x, bumper):
		self.x = x
		self.bumper = bumper
		self.dx = bumper.dx

	def move(self):
		self.x = self.x + self.dx
		return self.x
	
	def change_dx(self, dx):
		self.dx = dx
		
	def seeDeer(self, slow):
		#probability
		#acceleration change by fraction (slow)
		if np.random.randint(0,10) > 0 :
			self.dx = slow * self.dx
			print ("SEE DEER")
		

#New additions here
CarNumber = 1000
car_list = []
for i in range(CarNumber):
	new_car = Car()
	car_list.append(new_car)
#The above for loop creates a list of cars.
for n in range(CarNumber):
	if n-1>=0:
		car_list[n].x = car_list[n-1].x - (1)
		car_list[n].bumper = car_list[n-1]
#This above for loop sets the bumpers of each car (they watch the car in front of them) and makes a uniform distribution of cars on a 100-mile track by having every car after the first have a 'negative' position. Zero is when they enter the track.


#My job here is to make SeeDeer work.
t = 0
time = []
pos_list = []
for n in range(len(car_list)):
	pos_list.append([])
carselect = np.random.randint(0,(CarNumber - 1))
print "The",carselect,"th car has seen a deer."
car_list[carselect].seeDeer(.5)
#for n in range(len(car_list) - carselect):
	#car_list[carselect+n].dx = car_list[carselect].dx
#The above five lines choose a car to see a deer, tell me which car it is, and make every car behind it slow down.

while car_list[-1].x < 100:
	for n in range(len(car_list) - carselect):
		if 0<=car_list[carselect+n].x<=100:
			car_list[carselect+n].dx = car_list[carselect].dx
	for n in range(len(car_list)):
		car_list[n].move()
	for n in range(len(car_list)):
		pos_list[n].append(car_list[n].x)
	print car_list[-1].x
	t = t + 1
	time.append(t)
#print car_list[-1].x
#print car_list[0].x #This should be greater than 100.
#Ok, the above is working. Now I need to set it up so that a car only sees a deer for a limited time.
print "The time of completion is",t,"time steps."
print "The current problem is that the cars never speed back up after seeing a deer."