from matplotlib import pyplot as plt
import numpy as np
import matplotlib.cm as cm



class Car():
	
	def __init__(self, x=0, dx=5, bumper = None):
		self.x = x
		self.bumper = bumper
		self.dx = dx

	def move(self):
		self.x = self.x + self.dx
	
	def change_dx(self, dx):
		self.dx = dx

    def speedUp(self):
                self.dx = 5
                
	def seeDeer(self, slow, deerSeen):
        
		#probability
		#acceleration change by fraction (slow)
		if np.random.randint(0,10) > 1 and deerSeen == False:
                        self.dx = 1
                        #deerSeen =  True 
        #       else:
         #              self.x = self.x + self.dx
		

#New additions here
#in python, can do linked list functionality with lists, so no need to do linked list
CarNumber = 20
car_list = []
car_pos = []
graph_y = []
deer = []
yDeer = []

for i in range(0,CarNumber):
	if i == 0:
		new_car = Car(0, 5, None)
		car_list.append(new_car)
	else:
		new_car = Car(car_list[i-1].x - 10, car_list[i-1].dx, car_list[i-1])
		car_list.append(new_car)
	car_pos.append(new_car.x)
	graph_y.append(20)
deerSeen = False
while car_list[len(car_list) -1].x < 200 :
	print ("pos of last car: ", car_list[len(car_list) -1].x)
	del car_pos[:]
	del graph_y[:]
	#carselect = 3
        if car_list[len(car_list) -1].x % 5 == 0:
                deerSeen = False
                
	for i in range(0,CarNumber):
                if car_list[len(car_list) -1].x % 10 == 0:
                
                        carselect = np.random.randint(0,(CarNumber - 1))
                        #carselect = 0 
		        #if car_list[len(car_list) -1].x == 40:
		        if i == carselect:
				car_list[i].seeDeer(1,deerSeen)
                                deerSeen = True
                                print("Hi deer")
				#print carselect
                        if i > carselect:
                                #if carselect == 0:
                                        #car_list[i].change_dx(car_list[i+1].dx)
                               
                                        
                                car_list[i].change_dx(car_list[i-1].dx)
                        

                if deerSeen == False:
                        car_list[i].speedUp()
                        #if car_list[len(car_list)-1].x == 46:
                        #if i == carselect:
                        #car_list[i].speedUp()

		      
                        
		car_list[i].move()
		#print (car_list[i].dx)
		car_pos.append(car_list[i].x)
		graph_y.append(20)

	colours = cm.rainbow(np.linspace(0,1,len(graph_y)))
	plt.scatter(car_pos,graph_y,color = colours)
	plt.axis([0, 200, 10, 30])
	plt.draw()
	plt.pause(.1)
	plt.clf()
	
t = 0
time = []
carselect = np.random.randint(0,(CarNumber - 1))
print "The",carselect,"th car has seen a deer."
car_list[carselect].seeDeer(.5)
while car_list[-1].x < 100:
	for n in range(len(car_list) - carselect):
		if 0.<=car_list[n].x<=100.:
			car_list[carselect+n].dx = car_list[carselect].dx
	for n in range(len(car_list)):
		car_list[n].move()
	t = t + 1
	time.append(t)
print "The time of completion is",t,"time steps."